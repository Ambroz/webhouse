<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $register_form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'required' => 'true',
                'label' => false,
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Email'
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type'=>PasswordType::class,
                'required' => true,
                'invalid_message' => 'Hasła muszą być takie same',
                'first_options'  => [
                    'label' => false,
                    'error_bubbling' => true,
                    'attr'=>[
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Password'
                ]],
                'second_options' => [
                    'label' => false,
                    'attr'=>[
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Confirm Password'
                ]]
            ])
            ->add('imie', TextType::class, [
                'required' => 'true',
                'label' => false,
                'attr' => [
                     'class' => 'uk-margin uk-input',
                     'placeholder' => 'Imię'
                    ]
                ])
            ->add('nazwisko', TextType::class, [
                'required' => 'true',
                'label' => false,
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Nazwisko'
                    ]
                ])
            ->add('wydzial', TextType::class, [
                'required' => 'true',
                'label' => false,
                'attr' => [
                    'class' => 'uk-margin uk-input',
                    'placeholder' => 'Wydział'
                    ]
                ])
//            ->add('role', HiddenType::class, [
//                'attr' => [
//                    'value' => '[ROLE_USER]'
//                ]
//            ])
            ->add('is_active', HiddenType::class, [
                'attr' => [
                    'value' => '0'
                ]
            ])
            ->add('zarejestruj', SubmitType::class,[
                'attr' => [
                    'class' => 'uk-button uk-button-primary uk-float-right']
            ])
            ->getForm();

        $register_form->handleRequest($request);

        if($register_form->isSubmitted()) {
            if ($register_form->isValid()) {
                $data = $register_form->getData();
                $user = new User();

//            dump($data['second_options']);die;

                $user->setEmail($data['email']);
                $user->setPassword(
                    $passwordEncoder->encodePassword($user, $data['password'])
                );

                $user->setImie($data['imie']);
                $user->setNazwisko($data['nazwisko']);
                $user->setIsActive($data['is_active']);
                $user->setWydzial($data['wydzial']);
                $user->setRoles($user->getRoles());

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Utworzono użytkownika');

                return $this->redirect($this->generateUrl('main'));

            } else {
                $this->addFlash('danger', 'Wprowadzono błędne dane. Spróbuj ponownie');
            }
        }
        return $this->render('registration/register.html.twig', [
            'register_form' => $register_form->createView()
        ]);
    }
}
