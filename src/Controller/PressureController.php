<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PressureController extends AbstractController
{
    /**
     * @Route("/pressure", name="pres")
     */
    public function index()
    {
        return $this->render('measure/pressure.html.twig', [
            'controller_name' => 'PressureController',
        ]);
    }
}
