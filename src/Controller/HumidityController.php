<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HumidityController extends AbstractController
{
    /**
     * @Route("/humidity", name="hum")
     */
    public function index()
    {
        return $this->render('measure/humidity.html.twig', [
            'controller_name' => 'HumidityController',
        ]);
    }
}
