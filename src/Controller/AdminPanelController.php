<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminPanelController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_panel")
     */
    public function index()
    {
        return $this->render('admin_panel/admin_panel.html.twig', [
            'controller_name' => 'AdminPanelController',
        ]);
    }

    /**
     *  @Route("/admin/sensors", name="sensors")
     */
    public function sensor_status(){

    }

    /**
     * @Route("/admin/users", name="users")
     * @param UserRepository $userRepository
     * @return Response
     */
    public function user_data(UserRepository $userRepository){
        $login_user = $this->getUser()->getEmail();

        $userdata = $userRepository->getUserData($login_user);

        return $this->render('admin_panel/user_list.html.twig',
            [
            'userdata' => $userdata
        ]);
    }

    /**
     * @Route("/admin/users/change_status", name="change_status")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return RedirectResponse
     */
    public function change_user_status(Request $request, UserRepository $userRepository){

        $post = new User();
        $user_email = $request->get('user_email', $post);
        $user_status = $request->get('user_status', $post);

        $user = $userRepository->change_user_status($user_email, $user_status);
//        dump($user);die;
        return $this->redirect($this->generateUrl('users'));
    }
}
