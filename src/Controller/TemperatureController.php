<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TemperatureController extends AbstractController
{
    /**
     * @Route("/temperature", name="temp")
     */
    public function index()
    {

        $dataPoints = array();
        $y = 40;
        for ($i = 0; $i < 1000; $i++) {
            $y = rand(-30, 40);
            array_push($dataPoints, array("x" => $i, "y" => $y));
//            array_push($dataPoints, array($i => $y));

        }
        $newdata = json_encode($dataPoints);
//            dump($newdata);

            return $this->render('measure/temperature.html.twig',[
                'datapoint'=> $dataPoints
            ]);
    }
}
