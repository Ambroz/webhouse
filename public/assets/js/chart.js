
window.onload = function () {

    var chart = new CanvasJS.Chart("chartContainer", {
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        animationEnabled: true,
        zoomEnabled: true,
        title: {
            text: "Try Zooming and Panning"
        },
        axisY:{
            maximum: 40,
            minimum: -30
        },

        axisX:{
            maximum: 30,
            minimum: 1
        },
        data: [{
            type: "area",
            // dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>;
            dataPoints: chart_data

            }]
    });
chart.render();

}